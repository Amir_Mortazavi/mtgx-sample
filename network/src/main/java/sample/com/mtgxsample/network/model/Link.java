package sample.com.mtgxsample.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Link implements Parcelable {

	@SerializedName("viaplay:sections")
	private List<Section> mSections;

	public Link() {
	}

	public List<Section> getSections() {
		return mSections;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(mSections);
	}

	protected Link(Parcel in) {
		this.mSections = in.createTypedArrayList(Section.CREATOR);
	}

	public static final Creator<Link> CREATOR = new Creator<Link>() {
		public Link createFromParcel(Parcel source) {
			return new Link(source);
		}

		public Link[] newArray(int size) {
			return new Link[size];
		}
	};

	@Override
	public String toString() {
		return "Link{" +
				"sections=" + mSections +
				'}';
	}
}
