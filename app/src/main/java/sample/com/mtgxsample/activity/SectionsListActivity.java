package sample.com.mtgxsample.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import java.lang.ref.WeakReference;

import sample.com.mtgxsample.R;
import sample.com.mtgxsample.adapter.SectionsRecyclerAdapter;
import sample.com.mtgxsample.fragment.SectionDetailFragment;
import sample.com.mtgxsample.network.NetworkService;
import sample.com.mtgxsample.network.model.Section;
import sample.com.mtgxsample.network.model.SectionsList;
import sample.com.mtgxsample.network.request.GetSectionsListRequest;

public class SectionsListActivity extends AppCompatActivity implements SectionsRecyclerAdapter.SectionClickListener {

	private static final String TAG = SectionsListActivity.class.getSimpleName();
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private RecyclerView mRecyclerView;
	private ProgressBar mProgressBar;
	protected boolean mTwoPane;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sections_list);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setTitle(getTitle());

		mRecyclerView = (RecyclerView) findViewById(R.id.item_list);
		mProgressBar = (ProgressBar) findViewById(R.id.list_page_progress_bar);
		mRecyclerView.setAdapter(new SectionsRecyclerAdapter(this));
		if (findViewById(R.id.item_detail_container) != null) {
			// The detail container view will be present only in large-screen layouts (res/values-w900dp).
			// If this view is present, then the activity should be in two-pane mode.
			mTwoPane = true;
		}

		getSections();
	}

	private void getSections() {
		mProgressBar.setVisibility(View.VISIBLE);
		mRecyclerView.setVisibility(View.GONE);
		GetSectionsListRequest getSectionsListRequest = new GetSectionsListRequest(new GetSectionsSuccessListener(this), new GetSectionsErrorListener(this));
		NetworkService.getInstance(getApplicationContext()).addToRequestQueue(getSectionsListRequest, TAG);
	}

	@Override
	protected void onDestroy() {
		NetworkService.getInstance(getApplicationContext()).cancelPendingRequests(TAG);
		super.onDestroy();
	}

	@Override
	public void onSectionClicked(Section section, View view) {
		if (mTwoPane) {
			Bundle arguments = new Bundle();
			arguments.putParcelable(SectionDetailFragment.ARGS_SELECTED_SECTION, section);
			SectionDetailFragment fragment = new SectionDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().replace(R.id.item_detail_container, fragment).commit();
		} else {
			Context context = view.getContext();
			Intent intent = new Intent(context, SectionDetailActivity.class);
			intent.putExtra(SectionDetailFragment.ARGS_SELECTED_SECTION, section);
			context.startActivity(intent);
		}
	}

	private static class GetSectionsSuccessListener implements com.android.volley.Response.Listener<SectionsList> {
		private final WeakReference<SectionsListActivity> mSectionListActivityRef;

		public GetSectionsSuccessListener(SectionsListActivity sectionsListActivity) {
			this.mSectionListActivityRef = new WeakReference<>(sectionsListActivity);
		}

		@Override
		public void onResponse(SectionsList response) {
			final SectionsListActivity sectionsListActivity = mSectionListActivityRef.get();
			if (sectionsListActivity == null || sectionsListActivity.isFinishing()) {
				return;
			}
			sectionsListActivity.mProgressBar.setVisibility(View.GONE);
			sectionsListActivity.mRecyclerView.setVisibility(View.VISIBLE);
			SectionsRecyclerAdapter adapter = (SectionsRecyclerAdapter) sectionsListActivity.mRecyclerView.getAdapter();
			adapter.setSections(response.getSections());
		}
	}

	private static class GetSectionsErrorListener implements com.android.volley.Response.ErrorListener {
		private final WeakReference<SectionsListActivity> mSectionListActivityRef;

		public GetSectionsErrorListener(SectionsListActivity sectionsListActivity) {
			this.mSectionListActivityRef = new WeakReference<>(sectionsListActivity);
		}

		@Override
		public void onErrorResponse(VolleyError error) {
			final SectionsListActivity sectionsListActivity = mSectionListActivityRef.get();
			if (sectionsListActivity == null || sectionsListActivity.isFinishing()) {
				return;
			}
			sectionsListActivity.mProgressBar.setVisibility(View.GONE);
			sectionsListActivity.mRecyclerView.setVisibility(View.VISIBLE);
			if (error instanceof NoConnectionError) {
				Snackbar.make(sectionsListActivity.mRecyclerView, R.string.error_text_no_connection, Snackbar.LENGTH_LONG).show();
			} else {
				Snackbar.make(sectionsListActivity.mRecyclerView, R.string.error_text_general, Snackbar.LENGTH_LONG).show();
			}
			Log.e(TAG, "getSections onError: " + error);
		}
	}
}
