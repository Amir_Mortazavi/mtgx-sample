package sample.com.mtgxsample.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sample.com.mtgxsample.R;
import sample.com.mtgxsample.network.model.Section;

public class SectionsRecyclerAdapter extends RecyclerView.Adapter<SectionsRecyclerAdapter.ViewHolder> {

    private List<Section> mSections;
    private SectionClickListener mSectionClickListener;

    public SectionsRecyclerAdapter(SectionClickListener mSectionClickListener) {
        this.mSectionClickListener = mSectionClickListener;
    }

    public void setSections(List<Section> sections) {
        mSections = sections;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mSections.get(position);
        holder.mSectionName.setText(mSections.get(position).getTitle());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSectionClickListener.onSectionClicked(holder.mItem, view);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mSections == null ? 0 : mSections.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mSectionName;
        public Section mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mSectionName = (TextView) view.findViewById(R.id.section_name);
        }

    }

    public interface SectionClickListener {
        void onSectionClicked(Section section, View view);
    }
}
