package sample.com.mtgxsample.fragment;

import android.os.Bundle;
import android.view.View;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import sample.com.mtgxsample.BuildConfig;
import sample.com.mtgxsample.R;
import sample.com.mtgxsample.network.model.Section;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class SectionDetailFragmentTest {

	@Test
	public void testOnCreateView() throws Exception {

		SectionDetailFragment sectionDetailFragment = new SectionDetailFragment();
		Section dummySection = new Section("dummyTitle", "dummyLink");
		Bundle args = new Bundle();
		args.putParcelable(SectionDetailFragment.ARGS_SELECTED_SECTION, dummySection);
		sectionDetailFragment.setArguments(args);

		SupportFragmentTestUtil.startVisibleFragment(sectionDetailFragment);

		assertNotNull(sectionDetailFragment.getView());
		assertTrue(sectionDetailFragment.getView().findViewById(R.id.detail_page_progress_bar).getVisibility() == View.VISIBLE);
		assertTrue(sectionDetailFragment.getView().findViewById(R.id.section_detail_texts_layout).getVisibility() == View.GONE);
	}
}