package sample.com.mtgxsample.network.request;

import com.android.volley.Response;

import sample.com.mtgxsample.network.model.SectionsList;

public class GetSectionsListRequest extends GsonRequest<SectionsList> {

	private static final String URL = "https://content.viaplay.se/androidv2-se";

	public GetSectionsListRequest(Response.Listener<SectionsList> listener, Response.ErrorListener errorListener) {
		super(URL, SectionsList.class, listener, errorListener);
	}
}
