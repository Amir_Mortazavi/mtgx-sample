package sample.com.mtgxsample.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Section implements Parcelable {

	@SerializedName("id")
	private String mId;

	@SerializedName("title")
	private String mTitle;

	@SerializedName("href")
	private String mHref;

	public Section() {
	}

	public Section(String title, String href) {
		mTitle = title;
		mHref = href;
	}

	public String getId() {
		return mId;
	}

	public String getTitle() {
		return mTitle;
	}

	public String getHref() {
		return mHref;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.mId);
		dest.writeString(this.mTitle);
		dest.writeString(this.mHref);
	}


	private Section(Parcel in) {
		this.mId = in.readString();
		this.mTitle = in.readString();
		this.mHref = in.readString();
	}

	public static final Creator<Section> CREATOR = new Creator<Section>() {
		public Section createFromParcel(Parcel source) {
			return new Section(source);
		}

		public Section[] newArray(int size) {
			return new Section[size];
		}
	};

	@Override
	public String toString() {
		return "Section{" +
				"id='" + mId + '\'' +
				", title='" + mTitle + '\'' +
				", href='" + mHref + '\'' +
				'}';
	}
}
