package sample.com.mtgxsample.network.model;

import com.google.gson.annotations.SerializedName;

public class SectionDetails {

	@SerializedName("title")
	private String mTitle;

	@SerializedName("description")
	private String mDescription;

	public String getTitle() {
		return mTitle;
	}

	public String getDescription() {
		return mDescription;
	}

	@Override
	public String toString() {
		return "SectionDetails{" +
				"title='" + mTitle + '\'' +
				", description='" + mDescription + '\'' +
				'}';
	}
}
