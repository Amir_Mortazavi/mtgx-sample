package sample.com.mtgxsample.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import java.lang.ref.WeakReference;

import sample.com.mtgxsample.R;
import sample.com.mtgxsample.network.NetworkService;
import sample.com.mtgxsample.network.model.Section;
import sample.com.mtgxsample.network.model.SectionDetails;
import sample.com.mtgxsample.network.request.GetSectionDetailsRequest;

public class SectionDetailFragment extends Fragment {

	public static final String ARGS_SELECTED_SECTION = "selectedSection";
	public static final String TAG = SectionDetailFragment.class.getSimpleName();
	private Section mSection;
	private TextView mTitle;
	private View mTextLayout;
	private TextView mDetails;
	private ProgressBar mProgressBar;
	private Context mContext;


	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the fragment
	 */
	public SectionDetailFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_section_detail, container, false);
		mContext = getContext().getApplicationContext();
		mTitle = (TextView) rootView.findViewById(R.id.section_detail_title);
		mDetails = (TextView) rootView.findViewById(R.id.section_detail_description);
		mProgressBar = (ProgressBar) rootView.findViewById(R.id.detail_page_progress_bar);
		mTextLayout = rootView.findViewById(R.id.section_detail_texts_layout);
		if (getArguments().containsKey(ARGS_SELECTED_SECTION)) {
			mSection = getArguments().getParcelable(ARGS_SELECTED_SECTION);
			Activity activity = this.getActivity();
			CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
			if (appBarLayout != null) {
				appBarLayout.setTitle(mSection.getTitle());
			}
			getSectionDetails(mSection);
		}
		return rootView;
	}

	private void getSectionDetails(Section section) {
		mProgressBar.setVisibility(View.VISIBLE);
		mTextLayout.setVisibility(View.GONE);
		GetSectionDetailsRequest GetSectionDetailsRequest = new GetSectionDetailsRequest(section.getHref(), new GetSectionsDetailsSuccessListener(this), new GetSectionDetailsErrorListener(this));
		NetworkService.getInstance(mContext).addToRequestQueue(GetSectionDetailsRequest, TAG);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		NetworkService.getInstance(mContext).cancelPendingRequests(TAG);
	}

	private static class GetSectionsDetailsSuccessListener implements com.android.volley.Response.Listener<SectionDetails> {
		private final WeakReference<SectionDetailFragment> mPageDetailFragmentRef;

		public GetSectionsDetailsSuccessListener(SectionDetailFragment sectionDetailFragment) {
			this.mPageDetailFragmentRef = new WeakReference<>(sectionDetailFragment);
		}

		@Override
		public void onResponse(SectionDetails response) {
			final SectionDetailFragment sectionDetailFragment = mPageDetailFragmentRef.get();
			if (sectionDetailFragment == null || sectionDetailFragment.getActivity() == null) {
				return;
			}
			Log.d(TAG, "getSectionDetails onResponse " + response.getDescription());
			sectionDetailFragment.mProgressBar.setVisibility(View.GONE);
			sectionDetailFragment.mTextLayout.setVisibility(View.VISIBLE);
			sectionDetailFragment.mTitle.setText(response.getTitle());
			sectionDetailFragment.mDetails.setText(response.getDescription());
		}
	}

	private static class GetSectionDetailsErrorListener implements com.android.volley.Response.ErrorListener {
		private final WeakReference<SectionDetailFragment> mPageDetailFragmentRef;

		public GetSectionDetailsErrorListener(SectionDetailFragment sectionDetailFragment) {
			this.mPageDetailFragmentRef = new WeakReference<>(sectionDetailFragment);
		}

		@Override
		public void onErrorResponse(VolleyError error) {
			final SectionDetailFragment sectionDetailFragment = mPageDetailFragmentRef.get();
			if (sectionDetailFragment == null || sectionDetailFragment.getActivity() == null || sectionDetailFragment.getView() == null) {
				return;
			}
			sectionDetailFragment.mProgressBar.setVisibility(View.GONE);
			sectionDetailFragment.mTextLayout.setVisibility(View.VISIBLE);
			if (error instanceof NoConnectionError) {
				Snackbar.make(sectionDetailFragment.getView(), R.string.error_text_no_connection, Snackbar.LENGTH_LONG).show();
			} else {
				Snackbar.make(sectionDetailFragment.getView(), R.string.error_text_general, Snackbar.LENGTH_LONG).show();
			}
			Log.e(TAG, "getSectionDetails onError: " + error);
		}
	}
}
