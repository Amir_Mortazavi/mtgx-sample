package sample.com.mtgxsample.activity;

import android.content.Intent;
import android.view.MenuItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;

import sample.com.mtgxsample.BuildConfig;
import sample.com.mtgxsample.fragment.SectionDetailFragment;
import sample.com.mtgxsample.network.model.Section;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class SectionDetailActivityTest {

	private SectionDetailActivity activity;

	@Before
	public void setup() {
		Intent intent = new Intent();
		intent.putExtra(SectionDetailFragment.ARGS_SELECTED_SECTION, new Section("dummyTitle", "http://dummyLink"));
		activity = Robolectric.buildActivity(SectionDetailActivity.class).withIntent(intent).create().visible().get();
	}

	@Test
	public void testOnCreate() throws Exception {
		assertNotNull(Shadows.shadowOf(activity).getContentView());
		assertNotNull(activity);
	}

	@Test
	public void testOnOptionsItemSelected() throws Exception {
		MenuItem item = new RoboMenuItem(android.R.id.home);
		assertTrue(activity.onOptionsItemSelected(item));

		MenuItem unknownItem = new RoboMenuItem(android.R.id.button1);
		assertFalse(activity.onOptionsItemSelected(unknownItem));
	}
}
