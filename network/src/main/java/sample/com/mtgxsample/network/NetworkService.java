package sample.com.mtgxsample.network;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class NetworkService {

	private static final String TAG = NetworkService.class.getSimpleName();

	private static NetworkService mInstance;
	private RequestQueue mRequestQueue;
	private static Context mCtx;

	private NetworkService(Context context) {
		mCtx = context;
		mRequestQueue = getRequestQueue();
	}

	public static synchronized NetworkService getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new NetworkService(context);
		}
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			Log.d(TAG, "Initialize Volley RequestQueue!");
			mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
		}
		return mRequestQueue;
	}


	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

}



