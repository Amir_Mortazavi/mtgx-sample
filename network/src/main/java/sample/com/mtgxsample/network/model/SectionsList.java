package sample.com.mtgxsample.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

public class SectionsList {

	@SerializedName("_links")
	private Link mLink;

	public List<Section> getSections() {
		if (mLink != null) {
			return mLink.getSections();
		}
		return Collections.emptyList();
	}

	@Override
	public String toString() {
		return "SectionsList{" +
				"link=" + mLink +
				'}';
	}
}
