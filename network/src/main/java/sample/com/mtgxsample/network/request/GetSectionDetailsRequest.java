package sample.com.mtgxsample.network.request;

import com.android.volley.Response;

import sample.com.mtgxsample.network.model.SectionDetails;

public class GetSectionDetailsRequest extends GsonRequest<SectionDetails> {

	public GetSectionDetailsRequest(String url, Response.Listener<SectionDetails> listener, Response.ErrorListener errorListener) {
		super(url.replace("{?dtg}", ""), SectionDetails.class, listener, errorListener);
	}
}
