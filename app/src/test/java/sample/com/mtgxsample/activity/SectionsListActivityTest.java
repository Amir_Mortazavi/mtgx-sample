package sample.com.mtgxsample.activity;

import android.content.Intent;
import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowIntent;

import sample.com.mtgxsample.BuildConfig;
import sample.com.mtgxsample.network.model.Section;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.robolectric.Shadows.shadowOf;


@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class SectionsListActivityTest {

	private SectionsListActivity activity;

	@Before
	public void setup() {
		activity = Robolectric.buildActivity(SectionsListActivity.class).create().visible().get();
	}

	@Test
	public void testOnCreate() throws Exception {
		assertNotNull(activity);
	}

	@Test
	public void testOnSectionClickedInSamllScreens() throws Exception {
		activity.mTwoPane = false;
		Section mockSection = Mockito.mock(Section.class);
		View mockView = Mockito.mock(View.class);
		Mockito.when(mockView.getContext()).thenReturn(activity);

		activity.onSectionClicked(mockSection, mockView);

		Intent startedIntent = Shadows.shadowOf(activity).getNextStartedActivity();
		ShadowIntent shadowIntent = shadowOf(startedIntent);
		assertEquals(shadowIntent.getComponent().getClassName(), SectionDetailActivity.class.getCanonicalName());
	}

	@Test
	public void testOnSectionClickedInLargeScreens() throws Exception {
		activity.mTwoPane = true;
		Section mockSection = Mockito.mock(Section.class);
		View mockView = Mockito.mock(View.class);
		Mockito.when(mockView.getContext()).thenReturn(activity);

		activity.onSectionClicked(mockSection, mockView);

		Intent startedIntent = Shadows.shadowOf(activity).getNextStartedActivity();
		assertNull(startedIntent);
	}

}